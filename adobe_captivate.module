<?php

use Drupal\adobe_captivate\Plugin\Field\FieldFormatter\AdobeCaptivateUrlFormatter;

/**
 * @file
 * Adobe Captivate Field module adds a field for Adobe Captivate content.
 */

/**
 * Returns a list of standard Adobe Captivate sizes.
 *
 * @todo: This is based on Youtube and not sure if needed except for the responsive size.
 */
function adobe_captivate_size_options() {
  return [
    'responsive' => 'responsive (full-width of container)',
    '450x315' => '450px by 315px',
    '480x360' => '480px by 360px',
    '640x480' => '640px by 480px',
    '960x720' => '960px by 720px',
    'custom' => 'custom',
  ];
}

/**
 * Implements hook_theme().
 */
function adobe_captivate_theme($existing, $type, $theme, $path) {
  return [
    'adobe_captivate_embed' => [
      'variables' => [
        'adobe_captivate_url' => NULL,
        'entity_title' => NULL,
        'settings' => [],
      ],
    ],
  ];
}

/**
 * Prepares variables for the Adobe Captivate template.
 *
 * Default template: adobe-captivate-embed.html.twig.
 */
function template_preprocess_adobe_captivate_embed(&$variables) {
  $variables['adobe_captivate_url'] = AdobeCaptivateUrlFormatter::generateValidUrl($variables['adobe_captivate_url']);

  $variables['content_attributes']['src'] = $variables['adobe_captivate_url'];

  // Use the field's display settings to retrieve the dimensions.
  $size = $variables['settings']['adobe_captivate_size'];
  $width = $variables['settings']['adobe_captivate_width'];
  $height = $variables['settings']['adobe_captivate_height'];
  if ($size != 'responsive') {
    $dimensions = adobe_captivate_get_dimensions($size, $width, $height);
    // Assign the retrieved dimensions as attributes on the iframe element.
    $variables['content_attributes']['width'] = $dimensions['width'];
    $variables['content_attributes']['height'] = $dimensions['height'];
  }

  // Build the iframe element's title attribute value (for accessibility).
  $variables['content_attributes']['title'] = t('Embedded Adobe Captivate');
  if (!empty($variables['entity_title'])) {
    $variables['content_attributes']['title'] = t('Embedded Adobe Captivate for @entity_title', [
      '@entity_title' => $variables['entity_title'],
    ]);
  }

  // Alternative content for browsers that don't understand iframes (WCAG).
  $variables['content_attributes']['aria-label'] = $variables['content_attributes']['title'] . ': ' . $variables['adobe_captivate_url'];

  // Add classes to the wrapping element.
  $variables['attributes']['class'][] = 'adobe-captivate-container';
  if ($size == 'responsive') {
    // When the "responsive" size is chosen in the field's display settings,
    // this class is used by the module's CSS to make the player responsive.
    $variables['attributes']['class'][] = 'adobe-captivate-container--responsive';
  }
}

/**
 * Splits height & width when given size, as from adobe_captivate_size_options.
 *
 * @param string $size
 *   Image size.
 * @param string $width
 *   Image width.
 * @param string $height
 *   Image height.
 *
 * @return array
 *   An array containing the dimensions.
 */
function adobe_captivate_get_dimensions($size = NULL, $width = NULL, $height = NULL) {
  $dimensions = [];
  if ($size == 'custom') {
    $dimensions['width'] = (int) $width;
    $dimensions['height'] = (int) $height;
  }
  else {
    // Locate the 'x'.
    $strpos = strpos($size, 'x');
    // Width is the first dimension.
    $dimensions['width'] = substr($size, 0, $strpos);
    // Height is the second dimension.
    $dimensions['height'] = substr($size, $strpos + 1, strlen($size));
  }

  return $dimensions;
}
