<?php

/**
 * @file
 * Hooks and documentation related to Adobe Captivate Field module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Execute custom code after archive has been extracted.
 *
 * @param string $file_uri
 *   The file uri of the archive that was extracted.
 * @param string $directory
 *   The directory where archive was extracted.
 * @param Archiver $archiver
 *   The Archiver object used to extract the archive.
 */
function hook_adobe_captivate_after_extraction(&$file_uri, &$directory, Archiver &$archiver) {
}

/**
 * @} End of "addtogroup hooks".
 */
