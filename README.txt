SUMMARY - Adobe Captivate Field
===============================
The Adobe Captivate Field module provides a simple field that allows you to
embed Adobe Captivate to a content type, user, or any entity.


REQUIREMENTS
-------------
All dependencies of this module are enabled by default in Drupal 8.x.


INSTALLATION
-------------
Install this module as usual. Please see
http://drupal.org/documentation/install/modules-themes/modules-8


USAGE
-------
To use this module, create a new field of type 'Adobe Captivate URL' or
'Adobe Captivate File'.

You probably want to use Adobe Captivate File field, as it allows
uploading zip files. Zip file must contain the Adobe Captivate
files so, that the index.html is in the root of the archive.

If you are uploading the files manually somewhere, you can
use the Adobe Captivate URL field. You can enter the
URL there and it will be shown. The URL must end with index.html.


CONFIGURATION
--------------

The output of an Adobe Captivate URL and File fields can be manipulated in two
ways:
 * field-specific parameters found in that particular field's display settings
 * Views settings for the specific field

To configure the field settings:

 1. click 'Manage display' on the listing of entity types
 2. click the configuration gear to the right of the Adobe Captivate URL or
    File field


DEVELOPERS
----------

There's a Code Quality tool included in this module and to enable it, just run:

    php -d extension=iconv.so $(which composer) install

This will then scan code on every commit and find issues. It's not installed
globally, only for this project.
