<?php

namespace Drupal\adobe_captivate\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the 'adobe_captivate_upload' widget.
 *
 * @FieldWidget(
 *   id = "adobe_captivate_file_widget",
 *   label = @Translation("Adobe Captivate File"),
 *   field_types = {
 *     "adobe_captivate_file"
 *   },
 * )
 */
class AdobeCaptivateFileWidget extends FileWidget implements ContainerFactoryPluginInterface {

  /**
   * Form submission handler for upload/remove button of formElement().
   *
   * This runs in addition to and after file_managed_file_submit().
   *
   * @see file_managed_file_submit()
   */
  public static function submit($form, FormStateInterface $form_state) {
    parent::submit($form, $form_state);

    $button = $form_state->getTriggeringElement();
    // Go one level up in the form, to the widgets container.
    $element = NestedArray::getValue($form, array_slice($button['#array_parents'], 0, -1));

    $files = $element["#files"];

    $clicked_button = end($form_state->getTriggeringElement()['#parents']);

    // Extract Captivate files.
    foreach ($files as $file) {
      if ($clicked_button === 'upload_button') {
        $extract_directory = self::getExtractDirectory($file);
        try {
          self::extract($file->getFileUri(), $extract_directory);
        }
        catch (\Exception $e) {
          \Drupal::messenger()->addError($e->getMessage());
        }
      }
      elseif ($clicked_button === 'remove_button') {
        // @todo: Removing should be somewhere where the file entity if finally removed.
      }
    }
  }

  /**
   * Unpacks an archive file.
   *
   * @param string $file_uri
   *   The file uri of the archive you wish to extract.
   * @param string $directory
   *   The directory you wish to extract the archive into.
   *
   * @return Archiver
   *   The Archiver object used to extract the archive.
   *
   * @throws \Exception
   */
  private static function extract($file_uri, $directory) {
    $filepath = \Drupal::service('file_system')->realpath($file_uri);
    if (!is_file($filepath)) {
      throw new \Exception(t('Archivers can only operate on local files: %file not supported', ['%file' => $file_uri]));
    }
    $archiver = \Drupal::service('plugin.manager.archiver')->getInstance(['filepath' => $filepath]);

    if (file_exists($directory)) {
      try {
        \Drupal::service('file_system')->deleteRecursive($directory);
      }
      catch (FileException $e) {
        // Ignore failed deletes.
      }
    }

    $archiver->extract($directory);

    \Drupal::moduleHandler()->invokeAll(
      'adobe_captivate_after_extraction',
      [
        &$file_uri,
        &$directory,
        &$archiver,
      ]
    );

    return $archiver;
  }

  /**
   * Returns the directory where update archive files should be extracted.
   *
   * We could use static variables to detect the extraction directory
   * but if we ever make the target folder editable from field, then
   * it would be good to detect it. Basically there's the root folder where
   * Captivate files are saved and this is divided into 'archives' and
   * 'extracted' subfolders. This function gets the 'archives' path and changes
   * it into 'extracted' folder, appends file entity id plus always uses public
   * schema, because we can't view the extacted html files from private file
   * system.
   *
   * @param Drupal\file\Entity\File $file
   *   File entity.
   * @param bool $create
   *   (optional) Whether to attempt to create the directory if it does not
   *   already exist. Defaults to TRUE.
   *
   * @return string
   *   The full path to the temporary directory where update file archives
   *   should be extracted.
   */
  public static function getExtractDirectory(File $file, $create = TRUE) {
    $directory_adobe_captivate_root = dirname($file->getFileUri());
    $find = [
      'private',
      '/archives',
    ];
    $replace = [
      'public',
      '',
    ];
    $directory_adobe_captivate_root = str_replace($find, $replace, $directory_adobe_captivate_root);
    $extract_directory = $directory_adobe_captivate_root . '/extracted/' . $file->id();
    if ($create && !file_exists($extract_directory)) {
      \Drupal::service('file_system')->prepareDirectory($extract_directory, FileSystemInterface::CREATE_DIRECTORY);
    }
    return $extract_directory;
  }

}
