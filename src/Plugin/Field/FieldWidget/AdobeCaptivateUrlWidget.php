<?php

namespace Drupal\adobe_captivate\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'adobe_captivate_url' widget.
 *
 * @FieldWidget(
 *   id = "adobe_captivate_url_widget",
 *   label = @Translation("URL"),
 *   field_types = {
 *     "adobe_captivate_url"
 *   },
 * )
 */
class AdobeCaptivateUrlWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'placeholder_url' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['placeholder_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder for URL'),
      '#default_value' => $this->getSetting('placeholder_url'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $placeholder_url = $this->getSetting('placeholder_url');
    if (empty($placeholder_url)) {
      $summary[] = $this->t('No placeholders');
    }
    else {
      if (!empty($placeholder_url)) {
        $summary[] = $this->t('URL placeholder: @placeholder_url', ['@placeholder_url' => $placeholder_url]);
      }
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['adobe_captivate_url'] = $element + [
      '#type' => 'textfield',
      '#placeholder' => $this->getSetting('placeholder_url'),
      '#default_value' => isset($items[$delta]->adobe_captivate_url) ? $items[$delta]->adobe_captivate_url : NULL,
      '#maxlength' => 255,
      '#element_validate' => [[$this, 'validateInput']],
    ];

    if ($element['adobe_captivate_url']['#description'] == '') {
      $element['adobe_captivate_url']['#description'] = $this->t('Enter the Adobe Captivate URL. Valid URL format is: https://www.example.com/bar/index.html (must end with index.html).');
    }

    return $element;
  }

  /**
   * Validate Adobe Captivate URL.
   */
  public function validateInput(&$element, FormStateInterface &$form_state, $form) {
    // @codingStandardsIgnoreStart
    $input = trim($element['#value']);
    // @codingStandardsIgnoreEnd

    // Set trimmed input just to be sure we don't get
    // any weird output later when prepending https://.
    $form_state->setValueForElement($element, $input);

    if (!empty($input) && preg_match('/\/index\.html$/', $input) === 0) {
      $form_state->setError($element, $this->t('Enter the Adobe Captivate URL. Valid URL format is: https://www.example.com/bar/index.html (must end with index.html).'));
    }
  }

}
