<?php

namespace Drupal\adobe_captivate\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\adobe_captivate\Plugin\Field\FieldWidget\AdobeCaptivateFileWidget;

/**
 * Plugin implementation of the 'adobe_captivate_embed' formatter.
 *
 * @FieldFormatter(
 *   id = "adobe_captivate_embed",
 *   label = @Translation("Adobe Captivate embed"),
 *   field_types = {
 *     "adobe_captivate_url",
 *     "adobe_captivate_file"
 *   }
 * )
 */
class AdobeCaptivateFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'adobe_captivate_size' => 'responsive',
      'adobe_captivate_width' => '',
      'adobe_captivate_height' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['adobe_captivate_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Adobe Captivate embed size'),
      '#options' => adobe_captivate_size_options(),
      '#default_value' => $this->getSetting('adobe_captivate_size'),
    ];
    $elements['adobe_captivate_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#size' => 10,
      '#default_value' => $this->getSetting('adobe_captivate_width'),
      '#states' => [
        'visible' => [
          ':input[name*="adobe_captivate_size"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $elements['adobe_captivate_height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Height'),
      '#size' => 10,
      '#default_value' => $this->getSetting('adobe_captivate_height'),
      '#states' => [
        'visible' => [
          ':input[name*="adobe_captivate_size"]' => ['value' => 'custom'],
        ],
      ],
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $adobe_captivate_size = $this->getSetting('adobe_captivate_size');

    $summary[] = $this->t('Adobe Captivate embed: @adobe_captivate_size@', ['@adobe_captivate_size' => $adobe_captivate_size]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {}

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      $adobe_captivate_url = '';

      if ($item->getPluginId() === 'field_item:adobe_captivate_file') {
        // In case of 'field_item:adobe_captivate_file', we find the index.html
        // in files folder based on the location of uploaded zip.
        //
        $id = $item->getValue()['target_id'];
        $file = \Drupal::entityTypeManager()->getStorage('file')->load($id);
        $extract_directory = AdobeCaptivateFileWidget::getExtractDirectory($file, FALSE);

        if (file_exists($extract_directory . '/index.html')) {
          $adobe_captivate_url = $extract_directory . '/index.html';
        }

        // Change URI to URL.
        if ($adobe_captivate_url !== '') {
          $adobe_captivate_url = \Drupal::service('file_url_generator')->generateAbsoluteString($adobe_captivate_url);
        }
      }
      elseif ($item->getPluginId() === 'field_item:adobe_captivate_url') {
        // Otherwise the URL is is in the adobe_captivate_url field.
        //
        $adobe_captivate_url = $item->adobe_captivate_url;
      }

      // For some reason we dint't find the url to adobe
      // captivate - let's skip.
      if ($adobe_captivate_url === '') {
        continue;
      }

      $element[$delta] = [
        '#theme' => 'adobe_captivate_embed',
        '#adobe_captivate_url' => $adobe_captivate_url,
        '#entity_title' => $items->getEntity()->label(),
        '#settings' => $settings,
      ];

      $element[$delta]['#attached']['library'][] = 'adobe_captivate/drupal.adobe_captivate';

      if ($settings['adobe_captivate_size'] == 'responsive') {
        $element[$delta]['#attached']['library'][] = 'adobe_captivate/drupal.adobe_captivate.responsive';
      }
    }
    return $element;
  }

}
