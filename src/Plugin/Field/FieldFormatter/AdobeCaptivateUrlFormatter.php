<?php

namespace Drupal\adobe_captivate\Plugin\Field\FieldFormatter;

use Drupal\adobe_captivate\Plugin\Field\FieldWidget\AdobeCaptivateFileWidget;
use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'adobe_captivate_url' formatter.
 *
 * @FieldFormatter(
 *   id = "adobe_captivate_url",
 *   label = @Translation("Adobe Captivate URL"),
 *   field_types = {
 *     "adobe_captivate_url",
 *     "adobe_captivate_file"
 *   }
 * )
 */
class AdobeCaptivateUrlFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['link'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Output this field as a link'),
      '#default_value' => $this->getSetting('link'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $link = $this->getSetting('link');

    if ($link) {
      $summary[] = $this->t('Adobe Captivate URL as a link.');
    }
    else {
      $summary[] = $this->t('Adobe Captivate URL as plain text.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {}

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $link = $this->getSetting('link');

    foreach ($items as $delta => $item) {
      $url = '';

      if ($item->getPluginId() === 'field_item:adobe_captivate_file') {
        // In case of 'field_item:adobe_captivate_file', we find the index.html
        // in files folder based on the location of uploaded zip.
        //
        $uri = '';

        $id = $item->getValue()['target_id'];
        $file_entity = \Drupal::entityTypeManager()->getStorage('file')->load($id);
        $extract_directory = AdobeCaptivateFileWidget::getExtractDirectory($file_entity, FALSE);

        if (file_exists($extract_directory . '/index.html')) {
          $uri = $extract_directory . '/index.html';
        }

        if ($uri !== '') {
          $url = \Drupal::service('file_url_generator')->generate($uri);
        }
      }
      elseif ($item->getPluginId() === 'field_item:adobe_captivate_url') {
        // Otherwise the URL is in the input field.
        //
        $url = Url::fromUri(self::generateValidUrl($item->adobe_captivate_url));
      }

      // For some reason we dint't find the url to Adobe
      // Captivate - let's skip this iteration.
      if ($url === '') {
        continue;
      }

      if ($link) {
        $element[$delta] = [
          '#type' => 'link',
          '#title' => $url->toString(),
          '#url' => $url,
          '#options' => [
            'attributes' => [
              'class' => [
                'adobe-captivate-url',
              ],
            ],
            'html' => TRUE,
          ],
        ];
      }
      else {
        $element[$delta] = [
          '#markup' => Html::escape($item->input),
        ];
      }
    }

    return $element;
  }

  /**
   * Prepend protocol if missing and we assume it's https because it's 2020 :).
   */
  public static function generateValidUrl($url) {
    if (preg_match('#^https?://#', $url) === 0) {
      $url = 'https://' . $url;
    }

    return $url;
  }

}
