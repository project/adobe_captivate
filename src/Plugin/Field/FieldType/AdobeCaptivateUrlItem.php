<?php

namespace Drupal\adobe_captivate\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'adobe_captivate_url' field type.
 *
 * @FieldType(
 *   id = "adobe_captivate_url",
 *   label = @Translation("Adobe Captivate URL"),
 *   description = @Translation("This field stores an Adobe Captivate link in the database."),
 *   default_widget = "adobe_captivate_url_widget",
 *   default_formatter = "adobe_captivate_embed"
 * )
 */
class AdobeCaptivateUrlItem extends FieldItemBase {

  /**
   * Definitions of the contained properties.
   *
   * @var array
   */
  public static $propertyDefinitions;

  /**
   * {@inheritdoc}
   *
   * @todo: Is 1024 length enough?
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'adobe_captivate_url' => [
          'description' => 'Adobe Captivate URL.',
          'type' => 'varchar',
          'length' => 1024,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['adobe_captivate_url'] = DataDefinition::create('string')
      ->setLabel(t('Adobe Captivate URL'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('adobe_captivate_url')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'input';
  }

}
