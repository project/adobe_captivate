<?php

namespace Drupal\adobe_captivate\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the 'adobe_captivate_file' field type.
 *
 * @FieldType(
 *   id = "adobe_captivate_file",
 *   label = @Translation("Adobe Captivate File"),
 *   description = @Translation("This field stores the ID of a file as an integer value."),
 *   category = @Translation("Reference"),
 *   default_widget = "file_generic",
 *   default_formatter = "file_default",
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 */
class AdobeCaptivateFileItem extends FileItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'file_extensions' => 'zip',
      'file_directory' => 'adobe_captivate',
      'max_filesize' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    // Let's make extension hard-coded - we're only working with zip files.
    unset($element['file_extensions']);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected static function doGetUploadLocation(array $settings, $data = []) {
    $destination = trim($settings['file_directory'], '/') . '/archives';
    return $settings['uri_scheme'] . '://' . $destination;
  }

}
