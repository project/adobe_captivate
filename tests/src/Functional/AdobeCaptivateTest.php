<?php

namespace Drupal\Tests\adobe_captivate\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests adobe_captivate field widgets and formatters.
 *
 * @group adobe_captivate
 */
class AdobeCaptivateTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['adobe_captivate'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Create Basic page and Article node types.
    if ($this->profile != 'standard') {
      $this->drupalCreateContentType(['type' => 'page', 'name' => 'Basic page']);
    }

    $this->admin_user = $this->drupalCreateUser([
      'access content',
      'access administration pages',
      'administer site configuration',
      'administer content types',
      'administer nodes',
    ]);
    $this->drupalLogin($this->admin_user);
  }

  /**
   * Test 'adobe_captivate_url' field type.
   */
  public function testAdobeCaptivateUrl() {
    $field_name = mb_strtolower($this->randomMachineName());
    // Create a field.
    $field_storage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'translatable' => FALSE,
      'type' => 'adobe_captivate_url',
      'cardinality' => '1',
    ]);
    $field_storage->save();
    $field = \Drupal::entityTypeManager()->getStorage('field_config')->create([
      'field_storage' => $field_storage,
      'bundle' => 'page',
      'title' => DRUPAL_DISABLED,
    ]);
    $field->save();

    \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('node.page.default')
      ->setComponent($field_name, [
        'type' => 'adobe_captivate_url_widget',
        'settings' => [],
      ])
      ->save();

    \Drupal::entityTypeManager()->getStorage('entity_view_display')->load('node.page.default')
      ->setComponent($field_name, [
        'type' => 'adobe_captivate_embed',
      ])
      ->save();

    // Display creation form.
    $this->drupalGet('node/add/page');
    $this->assertSession()->fieldValueEquals("{$field_name}[0][adobe_captivate_url]", '');

    // Verify that a valid URL can be submitted.
    $value = 'http://example.com/foobar/index.html';
    $edit = [
      "title[0][value]" => 'Test',
      "{$field_name}[0][adobe_captivate_url]" => $value,
    ];
    $this->submitForm($edit, t('Save'));
    preg_match('|/node/(\d+)|', $this->getUrl(), $match);
    $this->assertSession()->pageTextContains(t('Basic page Test has been created.'));
    $this->assertSession()->responseContains($value);

    // Verify that the Adobe Captivate is displayed.
    $pattern = '<iframe.*src="' . $value;
    $pattern = '@' . $pattern . '@s';
    $this->assertSession()->responseMatches($pattern);

    // Verify that invalid URLs cannot be submitted.
    // In this case we're checking that it can't end with index.htm.
    $this->drupalGet('node/add/page');
    $incorrect_value = 'http://example.com/foobar/index.htm';
    $edit = [
      "title[0][value]" => 'Test1',
      "{$field_name}[0][adobe_captivate_url]" => $incorrect_value,
    ];
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains(t('Enter the Adobe Captivate URL. Valid URL format is: https://www.example.com/bar/index.html (must end with index.html).'));

    // Change to Adobe Captivate URL formatter.
    \Drupal::entityTypeManager()->getStorage('entity_view_display')->load('node.page.default')
      ->setComponent($field_name, [
        'type' => 'adobe_captivate_url',
      ])
      ->save();

    // Verify that the Adobe Captivate link is displayed.
    $this->drupalGet('node/1');
    $pattern = '<a.*href="' . $value . '".*class="adobe-captivate-url">.*</a>';
    $pattern = '@' . $pattern . '@s';
    $this->assertSession()->responseMatches($pattern);
  }

  /**
   * Test 'adobe_captivate_file' field type.
   */
  public function testAdobeCaptivateFile() {
    $field_name = mb_strtolower($this->randomMachineName());
    // Create a field.
    $field_storage = \Drupal::entityTypeManager()->getStorage('field_storage_config')->create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'translatable' => FALSE,
      'type' => 'adobe_captivate_file',
      'cardinality' => '1',
    ]);
    $field_storage->save();
    $field = \Drupal::entityTypeManager()->getStorage('field_config')->create([
      'field_storage' => $field_storage,
      'bundle' => 'page',
      'title' => DRUPAL_DISABLED,
    ]);
    $field->save();

    \Drupal::entityTypeManager()->getStorage('entity_form_display')->load('node.page.default')
      ->setComponent($field_name, [
        'type' => 'adobe_captivate_file_widget',
        'settings' => [],
      ])
      ->save();

    \Drupal::entityTypeManager()->getStorage('entity_view_display')->load('node.page.default')
      ->setComponent($field_name, [
        'type' => 'adobe_captivate_embed',
      ])
      ->save();

    // Display creation form.
    $this->drupalGet('node/add/page');
    $this->assertSession()->fieldValueEquals('files[' . $field_name . '_0]', '');

    // Verify that file can be submitted and node saved.
    $adobe_captivate_path = \Drupal::service('module_handler')->getModule('adobe_captivate')->getPath();
    $test_zip_path = $this->root . '/' . $adobe_captivate_path . '/tests/aaa_adobe_captivate_test.zip';
    $edit = [
      'title[0][value]' => 'Test',
      'files[' . $field_name . '_0]' => $test_zip_path,
    ];
    $this->submitForm($edit, t('Upload'));
    $this->submitForm([], t('Save'));

    // Don't know how to find the Simpletest baseurl so generating partial url.
    // @todo. Should be possible after https://www.drupal.org/project/drupal/issues/471970.
    $iframe_src_partial = '/files/adobe_captivate/extracted/1/index.html';

    // Verify that the Adobe Captivate is displayed.
    // This also makes sure that the index.html exists because iframe is not shown if file does not exist.
    $this->drupalGet('node/1');
    $pattern = '<iframe.*src=".*' . $iframe_src_partial . '"';
    $pattern = '@' . $pattern . '@s';
    $this->assertSession()->responseMatches($pattern);

  }

}
